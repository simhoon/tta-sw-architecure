# 8. UI Component Library

Date: 2021-04-05

## Status

Proposed

## Context

1. Design System이 있어야 한다.
2. 아직 Design Concept, 시안 자체도 정해지지 않은 상태이다.
3. 짧은 개발 기간을 고려하면, 생산성 높은 component library를 택해 개발을 시작하고, 그 base로 design 작업을 해야지만 그나마 개발 완료가 가능하다.
4. 후보군
  - BootstrapVue : 가장 널리 쓰인 bootstrap 기반이며, 너무 흔한 모양이 되며, customizing이 쉽지 않다.
  - Vuetify : Material 계열이며, 안드로이드쪽에서 밀고 있으나, 호불호가 갈린다.
  - Beufy : Bulma 계열이며, 요즘 TailWind와 함께 급성장하고 있다.
  - TailWindVue : TailWind 계열이며, 가장 핫한 CSS framework을 채용한 component library이다.
  - Quasar : 기업용 화면에 적합하여 많은 사용자들을 가지고 있고, 문서화가 잘 되어 있다.
  - Element UI : 중국쪽의 적극적인 지원을 받고 있으나, 문서가 중국어다.

## Decision

Quasar UI component library를 사용한다.

## Consequences

Quasar를 기반으로 기본 화면 구성 시작하며, 새 Component들이 개발되는 대로 이를 하나씩 치환해 나간다.
